# Video Removal Application  
  
This is a Demo Application that creates a Simple web page with a table for the videos already added in the application and a form to submit new videos. The user may opt to remove the silences when adding new videos or change them later.  
  
### Current constrains:  
The database is in memory, we can change it to mysql or postgreSQL in the future.  
The video files are store in the tmp folder, as a better solution we should use an S3 mount.  
  
**Note**: When restarting the docker container it will lose the information.  
  
### Improvements:  
In order to make a better user experience in the future change the image processing to a background task, using **Celery** for example.  

### Install process:
**Build**
On the project folder /path/to/video-removal
> $ docker build . -t video-removal:latest
  
**Run**  
> $ docker run -d -p 5000:5000 video-removal:latest

Go to http://0.0.0.0:5000/