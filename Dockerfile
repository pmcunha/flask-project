FROM jfloff/alpine-python:3.6-slim
RUN apk update
RUN apk add ffmpeg
VOLUME /tmp
COPY . /app
WORKDIR /app
RUN pip install -r requirements.txt --default-timeout=100
ENTRYPOINT ["python"]
CMD ["run.py"]