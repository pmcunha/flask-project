from core import create_app

import settings

create_app().run(debug=settings.DEBUG, host='0.0.0.0')
