# Database settings
SQLALCHEMY_DATABASE_URI = 'sqlite:////tmp/test.db'
SQLALCHEMY_TRACK_MODIFICATIONS = True

# Video Settings
ALLOWED_EXTENSIONS = set(['mp4'])
UPLOAD_FOLDER = '/tmp/videos-removal'

SECRET_KEY = 'wHNwYaqU83SR8dIfTIEH'
WTF_CSRF_ENABLED = True

DEBUG = False