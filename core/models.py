
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


class Video(db.Model):

    id = db.Column(db.String(80), primary_key=True)

    title = db.Column(db.String(80), nullable=False)

    without_silence = db.Column(db.Boolean, nullable=False, default=False)

    filename = db.Column(db.String(80), nullable=False)

    def __repr__(self):
        return '<Video {0} Title: "{1}">'.format(self.filename, self.title)

