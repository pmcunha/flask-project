from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileRequired, FileAllowed
from wtforms import StringField, BooleanField, validators

import settings


class SubmitFileForm(FlaskForm):

    title = StringField('Title', [validators.Length(min=4, max=80), validators.InputRequired()])

    video = FileField('File', validators=[FileRequired(),
                                          FileAllowed(settings.ALLOWED_EXTENSIONS)])

    without_silence = BooleanField('Remove Silence')
