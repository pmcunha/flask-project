from flask import Flask
from flask_bootstrap import Bootstrap

import settings
from .models import db
from .backend.routes import mod as backend_mod
from .frontend.routes import mod as frontend_mod

app = Flask(__name__)

app.register_blueprint(frontend_mod)
app.register_blueprint(backend_mod, url_prefix='/api')


def create_app():

    # Database settings
    app.config['SQLALCHEMY_DATABASE_URI'] = settings.SQLALCHEMY_DATABASE_URI
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = settings.SQLALCHEMY_TRACK_MODIFICATIONS

    # Video Settings
    app.config['UPLOAD_FOLDER'] = settings.UPLOAD_FOLDER

    app.config['WTF_CSRF_ENABLED'] = settings.WTF_CSRF_ENABLED
    app.config['SECRET_KEY'] = settings.SECRET_KEY

    Bootstrap(app)

    db.init_app(app)
    db.create_all(app=app)

    return app

