from flask import Blueprint, redirect, url_for, flash

from core.utils import delete_video, get_video_to_download, process_video

mod = Blueprint("backend", __name__)


@mod.route('/videos/<video_id>/download/', methods=['GET'])
def get_video(video_id):
    try:
        video = get_video_to_download(video_id)
    except Exception as e:
        flash('An error happen while downloading the video. Please try again later.', category='danger')
        return redirect(url_for('frontend.home_page'))
    else:
        return video


@mod.route('/videos/<video_id>/delete/', methods=['GET'])
def delete_video_by_id(video_id):
    try:
        delete_video(video_id)
        flash('Video deleted with success', category='success')
    except Exception as e:
        flash('An error happen while deleting. Please try again later.', category='danger')
    return redirect(url_for('frontend.home_page'))


@mod.route('/videos/<video_id>/process-video/', methods=['GET'])
def process_video_by_id(video_id):
    try:
        process_video(video_id)
        flash('Video process with success', category='success')
    except Exception as e:
        flash('An error happen while processing. Please try again later.', category='danger')
    return redirect(url_for('frontend.home_page'))


@mod.route('/videos/<video_id>/download-processed/', methods=['GET'])
def get_video_without_silence(video_id):
    try:
        video = get_video_to_download(video_id, 'processed')
        flash('Video process with success', category='success')
    except Exception as e:
        flash('An error happen while processing. Please try again later.', category='danger')
        return redirect(url_for('frontend.home_page'))
    else:
        return video
