from flask import Blueprint, render_template, request, flash

from core.forms import SubmitFileForm
from core.utils import get_videos_dict, save_video

mod = Blueprint("frontend", __name__)


@mod.route('/', methods=['GET', 'POST'])
def home_page():
    form = SubmitFileForm()
    if request.method == 'POST' and form.validate_on_submit():
        try:
            save_video(form.title.data, form.video.data, form.without_silence.data)
            flash('Video saved with success', category='success')
        except Exception as e:
            flash('An error happen while adding the video. Please try again later.', category='danger')
    return render_template('index.html', videos=get_videos_dict(), form=form)

