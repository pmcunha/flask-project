import io
import os
import shutil
import subprocess
import uuid

from flask import send_file
from pydub import AudioSegment, silence
from werkzeug.utils import secure_filename

import settings
from core.models import Video, db

import wave


def save_video(title, video, without_silence):
    """Function to save the video information, returns the video ID"""
    video_id = str(uuid.uuid4())
    filename = secure_filename(video.filename)

    add_video_database(video_id, title, filename, False)

    if not os.path.exists(os.path.join(settings.UPLOAD_FOLDER, video_id)):
        os.makedirs(os.path.join(settings.UPLOAD_FOLDER, video_id))

    video.save(os.path.join(settings.UPLOAD_FOLDER, video_id, filename))

    if without_silence:  # If user already want the video processed
        process_video(video_id)


def add_video_database(video_id, title, filename, without_silence):
    """Function to insert the video on the Database"""
    video = Video(id=video_id, title=title,
                  without_silence=without_silence,
                  filename=filename)
    db.session.add(video)
    db.session.commit()


def delete_video(video_id):
    """Function to delete video from the Database"""
    video = get_video_obj(video_id)

    path = os.path.join(settings.UPLOAD_FOLDER, video_id)  # Remove path for video
    shutil.rmtree(path)

    db.session.delete(video)
    db.session.commit()


def get_videos_dict():
    """Function that gets the videos entries from the Database and creates a dictionary"""
    return [{'id': video.id,
             'title': video.title,
             'without_silence': video.without_silence,
             'filename': video.filename,
             }
            for video in Video.query.all()]


def get_video_obj(video_id):
    """Function to get the video from the Database"""
    video = Video.query.filter_by(id=video_id).first()
    if video is None:
        raise Exception('Video not found')
    return video


def get_video_to_download(video_id, prefix=''):
    """Function to get the video file and respond the information"""
    video = get_video_obj(video_id)
    filename = '{0}-{1}'.format(prefix, video.filename) if prefix != '' else video.filename
    f = open(os.path.join(settings.UPLOAD_FOLDER, video_id, filename), 'rb')
    return send_file(io.BytesIO(f.read()),
                     as_attachment=True,
                     attachment_filename=filename,
                     mimetype='	video/mp4')


def process_video(video_id):
    """Function to extract the muted parts"""
    video = get_video_obj(video_id)
    separate_audio_from_video(video)
    audio_silence_parts, total_len = detects_silence_in_audio(video)
    remove_parts_from_video(video, audio_silence_parts, total_len)
    video.without_silence = True
    db.session.commit()


def separate_audio_from_video(video):
    """Function to get the sound file and store in WAV format"""
    file_path_input = os.path.join(settings.UPLOAD_FOLDER, video.id, video.filename)
    file_path_output = os.path.join(settings.UPLOAD_FOLDER,
                                    video.id,
                                    '{0}-{1}.wav'.format('sound-only', video.filename.split('.')[0]))

    if os.path.exists(file_path_output):
        os.remove(file_path_output)

    subprocess.call(['ffmpeg', '-i', file_path_input, '-codec:a', 'pcm_s16le', '-ac', '1', file_path_output])


def detects_silence_in_audio(video):
    """Function that detects silences in sound file"""
    file_path_output = os.path.join(settings.UPLOAD_FOLDER,
                                    video.id,
                                    '{0}-{1}.wav'.format('sound-only', video.filename.split('.')[0]))

    audio_silence = []
    with wave.open(file_path_output, 'rb') as f:
        total_len = round(f.getnframes()/f.getframerate())

        for num in range(1, total_len):
            frame = f.readframes(f.getframerate())
            if all(map(lambda x: x == 0, frame)):
                audio_silence.append((num, num+1))

    return audio_silence, total_len


def select_parts_to_cut(audio_silence_parts, total_len):
    """Function to get the parts to cut from the file"""
    parts = []
    end_time = total_len
    for elem in reversed(audio_silence_parts):
        if end_time != elem[1]:
            parts.append((elem[1], end_time))
        end_time = elem[0]
    return list(reversed(parts))


def remove_parts_from_video(video, audio_silence_parts, total_len):
    """Function to remove the parts in the video where there is silence"""
    # Init the paths to work
    file_path_input = os.path.join(settings.UPLOAD_FOLDER, video.id, video.filename)
    file_path_output = os.path.join(settings.UPLOAD_FOLDER, video.id, '{0}-{1}'.format('processed', video.filename))
    parts_file = os.path.join(settings.UPLOAD_FOLDER, video.id, 'parts.txt')

    for num, elem in enumerate(select_parts_to_cut(audio_silence_parts, total_len)):
        filename = '{0}_{1}_{2}'.format('part', str(num + 1), video.filename)
        file_path_tmp = os.path.join(settings.UPLOAD_FOLDER, video.id, filename)

        subprocess.call(['ffmpeg', '-ss', humanize_time(elem[0]), '-i', file_path_input,
                         '-t', humanize_time(elem[1] - elem[0]),
                         '-c:v', 'copy', '-c:a', 'copy', file_path_tmp]) # Cut part from video
        with open(parts_file, 'a') as f:
            f.write("file '{0}'\n".format(filename))

    subprocess.call(['ffmpeg', '-f', 'concat', '-i', parts_file, '-c', 'copy', file_path_output])

    # Clean Folder
    for num, elem in enumerate(select_parts_to_cut(audio_silence_parts, total_len)):
        filename = '{0}_{1}_{2}'.format('part', str(num + 1), video.filename)
        file_path_tmp = os.path.join(settings.UPLOAD_FOLDER, video.id, filename)
        os.remove(file_path_tmp)

    os.remove(parts_file)


def humanize_time(secs):
    """Function to humanize the seconds in date"""
    mins, secs = divmod(secs, 60)
    hours, mins = divmod(mins, 60)
    return '%02d:%02d:%02d' % (hours, mins, secs)


