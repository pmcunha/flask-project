import os
import sys
import unittest

from mock import patch, Mock
from flask import Flask
from flask_testing import TestCase
from werkzeug.datastructures import FileStorage

sys.path.insert(0, os.path.abspath('..'))  # Get around import problems

from core.backend.routes import mod as backend_mod
from core.frontend.routes import mod as frontend_mod
from core.utils import get_video_obj, add_video_database, get_videos_dict, delete_video, save_video, \
    humanize_time, select_parts_to_cut
from core.models import db, Video


class BasicUnitTests(TestCase):

    def create_app(self):
        app = Flask(__name__)

        app.register_blueprint(frontend_mod)
        app.register_blueprint(backend_mod, url_prefix='/api')

        # Database settings
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///unit-test.db'
        app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
        app.config['TESTING'] = True

        db.init_app(app)
        return app

    def setUp(self):
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    @patch('core.utils.add_video_database')
    @patch('core.utils.process_video')
    @patch('os.makedirs')
    def test_save_video_no_process_video(self, mock_makedirs, mock_process_video, mock_add_video_database):
        video_file = Mock(spec=FileStorage)
        video_file.filename = 'abcd.mp4'
        video_file.save.return_value = None

        video_id = save_video('Title', video_file, False)
        self.assertTrue(video_file.save.called)
        self.assertTrue(mock_makedirs.called)
        self.assertTrue(mock_add_video_database.called)

        self.assertFalse(mock_process_video.called)

    @patch('core.utils.add_video_database')
    @patch('core.utils.process_video')
    @patch('os.makedirs')
    def test_save_video_with_process_video(self, mock_makedirs, mock_process_video, mock_add_video_database):
        video_file = Mock(spec=FileStorage)
        video_file.filename = 'abcd.mp4'
        video_file.save.return_value = None

        video_id = save_video('Title', video_file, True)
        self.assertTrue(video_file.save.called)
        self.assertTrue(mock_makedirs.called)
        self.assertTrue(mock_add_video_database.called)

        self.assertTrue(mock_process_video.called)

    def test_add_video_database(self):
        add_video_database('12345', 'Test', 'abcd.mp4', False)
        video = Video.query.filter_by(id='12345').first()
        self.assertEqual('Test', video.title)

    @patch('shutil.rmtree')
    def test_delete_video(self, mock_rmtree):
        self.test_add_video_database()
        delete_video('12345')
        self.test_get_video_obj_not_found()
        self.assertTrue(mock_rmtree.called)

    def test_get_videos_dict(self):
        self.test_add_video_database()
        video = get_videos_dict()
        self.assertEqual('Test', video[0]['title'])
        self.assertEqual(False, video[0]['without_silence'])
        self.assertEqual('abcd.mp4', video[0]['filename'])

    def test_get_video_obj_not_found(self):
        with self.assertRaises(Exception) as e:
            get_video_obj('12345')
        self.assertEqual(e.exception.args[0], 'Video not found')

    def test_get_video_obj_exists(self):
        self.test_add_video_database()
        video = get_video_obj('12345')
        self.assertEqual('Test', video.title)

    def test_select_parts_to_cut(self):
        self.assertEqual([(7, 12), (14, 128)], select_parts_to_cut([(2, 7), (12, 14), (128, 129)], 129))

    def test_humanize_time(self):
        self.assertEqual('00:00:10', humanize_time(10))
        self.assertEqual('00:02:10', humanize_time(130))
        self.assertEqual('01:00:10', humanize_time(3610))


if __name__ == "__main__":
    unittest.main()