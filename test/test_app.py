import os
import sys
import unittest

import requests

from flask import Flask
from flask_bootstrap import Bootstrap
from flask_testing import LiveServerTestCase

sys.path.insert(0, os.path.abspath('..'))  # Get around import problems

from core.backend.routes import mod as backend_mod
from core.frontend.routes import mod as frontend_mod
from core.models import db


class IntegrationTests(LiveServerTestCase):

    render_templates = False

    def create_app(self):
        app = Flask(__name__, template_folder='../core/templates')

        app.register_blueprint(frontend_mod)
        app.register_blueprint(backend_mod, url_prefix='/api')

        # Database settings
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///unit-test.db'
        app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
        app.config['TESTING'] = True

        Bootstrap(app)

        app.config['SECRET_KEY'] = 'SECRET_KEY'

        db.init_app(app)
        return app

    def setUp(self):
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    def test_server_is_up_and_running(self):
        response = requests.get(self.get_server_url())
        self.assertEqual(response.status_code, 200)

    def test_api_video_download_no_video(self):
        response = requests.get(self.get_server_url() + '/api/videos/1234/download/')
        self.assertEqual(response.url, self.get_server_url() + '/')
        self.assertEqual(response.status_code, 200)

    def test_api_video_delete_no_video(self):
        response = requests.get(self.get_server_url() + '/api/videos/1234/delete/')
        self.assertEqual(response.url, self.get_server_url() + '/')
        self.assertEqual(response.status_code, 200)

    def test_api_video_process_video_no_video(self):
        response = requests.get(self.get_server_url() + '/api/videos/1234/process-video/')
        self.assertEqual(response.url, self.get_server_url() + '/')
        self.assertEqual(response.status_code, 200)

    def test_api_video_download_processed_video_no_video(self):
        response = requests.get(self.get_server_url() + '/api/videos/1234/process-video/')
        self.assertEqual(response.url, self.get_server_url() + '/')
        self.assertEqual(response.status_code, 200)


if __name__ == "__main__":
    unittest.main()